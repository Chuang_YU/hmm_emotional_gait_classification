# -*- coding: utf-8 -*-
"""
Spyder Editor- Python 3.6 at Anaconda
This is a temporary script file.
Chuang YU in ENSTA Email: alexchauncy@gmail.com
"Make the math and code easier"
HMM for emotional gait classification
"""
import warnings
import numpy as np
from hmmlearn import hmm
import pandas as pd
from sklearn.model_selection import train_test_split

#train_size for test train set split...............you can change here for how many data as train data
train_size_changable=280

#XyDatabase
# X type:array [75*56,8]
gaitdata_1_DataFrame=pd.read_csv("E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\gait_data_base_2400_56_after_medfilt.csv")
gaitdata_2=gaitdata_1_DataFrame.values[:,:]
gaitdata_3=gaitdata_2.reshape(300,8,56)
X=gaitdata_3
y=np.array([0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3])

# normalization.............................................................
feature_maxs=[66.6875768653487,28.00530179872793,98.1741941489158,20.2249244064732,212.51638189202203,14.460468972020012,215.063469854803,31.954409878614]
feature_mins=[0.29858782013189,-19.969585800811398,0.22346874037266198,-22.814995502244702,131.05306266479099,-14.23406094317599,131.345011105322,-33.45968473706398]
def norm_func (x0,min0,max0):
    y0=(x0-min0)/(max0-min0)
    return y0
#X normalization
for ii in range(300):
    for jj in range(8):
        X[ii,jj,:]=norm_func(X[ii,jj,:],feature_mins[jj],feature_maxs[jj])
        
# X_all---->divide into "X_train,X_test"
X_train,X_test,y_train,y_test=train_test_split(X, y, train_size =train_size_changable, random_state=1, stratify=y) 
# random_state=int means evert time get different data, shuttle=true means disorder them  
#stratify=y: input data 2:2:2 test data will be 1:1:1

#function reshape to match the GaussianHMM model
def reshape_func(A):#input_number:how many [8,56]s
    for i in np.arange(len(A)):
        if i==0:
            B=A[i,:,:].T
        if i>=1:
            B=np.vstack((B,A[i,:,:].T))
    print("size:",B.size, "shape:",B.shape,"type:",type(B))
    return B

#define a function for GaussianHMM

def Model_HMM(state_number,X_train_f,X_lens_f):
    #hmm model training 
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    model_f=hmm.GaussianHMM(n_components=state_number,
                            n_iter=100,
                            tol=0.1,#tol: The convergence threshold. 
                            #EM iterations will stop when the lower bound average gain is below this threshold.
                            covariance_type="full") #each component has its own general covariance matrix
    model_f.fit(X_train_f,X_lens_f)
    #print (model_f.transmat_)
    #     logL = model.score(X, lengths)
#     return model, logL
    logL= model_f.score(X_train_f,X_lens_f)
    return model_f, logL 

#based on emotion_flag(0,1,2,3) to get the netural,happy,angry,sad train_data_bases
def emotionl_train_data(X_train_f,y_train_f,emotion_flag):
    flag_0=True
    for i in np.arange(len(y_train_f)):
        if y_train_f[i]==emotion_flag:
            if flag_0==True:
                flag_0=False
                X_train_emotion_f=X_train_f[i,:,:].reshape([1,8,56])# X_train_f[i,:,:].shape=[8,56]
            else:
                X_train_emotion_f=np.vstack([X_train_emotion_f,X_train_f[i,:,:].reshape([1,8,56])])
    return X_train_emotion_f 

########################################TRAIN HMM Model**************************************

# four Gaussian models for four emotions(0:netural, 1:happy, 2:angry, 3:sad)
X_lengths=[56]*(int)(train_size_changable/4)

#*******************************************************************************************************
# 0:netural
# netural training data: X_train_netural
X_train_netural_0= emotionl_train_data(X_train,y_train,0)
# reshape the data 
X_train_netural= reshape_func(X_train_netural_0)
#train HMM model
state_num_n=4
hmm_model_n,logL_n=Model_HMM(state_num_n,X_train_netural,X_lengths)

#*******************************************************************************************************
# 1:happy
X_train_happy_0= emotionl_train_data(X_train,y_train,1)
X_train_happy=reshape_func(X_train_happy_0)
state_num_h=4
hmm_model_h,logL_h=Model_HMM(state_num_h,X_train_happy,X_lengths)


#*******************************************************************************************************
# 2:angry
X_train_angry_0= emotionl_train_data(X_train,y_train,2)
X_train_angry=reshape_func(X_train_angry_0)
state_num_a=4
hmm_model_a,logL_a=Model_HMM(state_num_a,X_train_angry,X_lengths)


#*******************************************************************************************************
# 3:sad
X_train_sad_0= emotionl_train_data(X_train,y_train,3)
X_train_sad=reshape_func(X_train_sad_0)
state_num_s=4
hmm_model_s,logL_s=Model_HMM(state_num_s,X_train_sad,X_lengths)


########################################positive TEST HMM Model**************************************
#X_test_demo=X_train_sad[56:112,:]
#score1=hmm_model_s.score(X_test_demo)
#score2=hmm_model_h.score(X_test_demo)
#score3=hmm_model_a.score(X_test_demo)
#score4=hmm_model_n.score(X_test_demo)
#print("score_match",score1)
#print("score_not_match",score2)
#print("score_not_match",score3)
#print("score_not_match",score4)
# put the 4 model into HMM_models
HMM_models=[hmm_model_n,hmm_model_h,hmm_model_a,hmm_model_s]
#test total number
test_size_changable=300-train_size_changable#test_size_changable=100
#the right number
test_right_times=0
#Scores[] to save the test scores
Scores=[0,0,0,0] 
#Transfer X_test into test data
X_test_transfer=reshape_func(X_test)
for i in np.arange(test_size_changable):
    X_test_one=X_test_transfer[(i*56):(i*56+56),:]
    for j in np.arange(4):
        Scores[j]=HMM_models[j].score(X_test_one)
    max_index=Scores.index(max(Scores))
    print("max_inex",max_index)
    if y_test[i]==max_index:
        test_right_times=test_right_times+1
acc_hmm_test=test_right_times/test_size_changable
print("right_times:",test_right_times,"/",test_size_changable)
print("acc_hmm: %",acc_hmm_test*100)



# =============================================================================
#test the data and to get the 
# test2 = np.array([[2, 1,0,2,1,0,3]]).T
# score = model.score(test2) #https://blog.csdn.net/u011311291/article/details/79131298
